import re

FILENAME = "log.log"

try:
    with open(FILENAME, 'r') as logfile:
        records = [line.strip() for line in logfile.readlines()]
except (FileNotFoundError, OSError):
    print(f"Could not open file {FILENAME}, terminating")
    exit(1)

# we only need ips and traffic, so everything else can be safely dropped
cleaner_records = [re.sub(r'\[.*?\]', '', line) for line in records]
cleaner_records = [re.sub(r'\".*?\"', '', line) for line in cleaner_records]

print(cleaner_records)

# now we can split them by whitespaces and get what we need
# each line looks like this: '195.77.230.188 - -   200 1793 0.009    request_id=-'
# so after split IP will be the first element, and the traffic amount the sixth

ips_traffic = dict()

for line in cleaner_records:
    split_line = line.split(" ")
    ip = split_line[0]
    traffic = split_line[6]
    ips_traffic[ip] = ips_traffic.get(ip, 0) + int(traffic)

print(ips_traffic)
# got the traffic. Sort it and output
results = sorted(ips_traffic.items(), key=lambda x: x[1], reverse=True)[:10]

for result in results:
    print(result)

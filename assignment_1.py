import requests


def load_urls(filepath):
    urls = list()
    try:
        with open(filepath, 'r') as urls_file:
            urls = [line.strip() for line in urls_file.readlines()]
    except (FileNotFoundError, OSError) as e:
        print(f"Exception: {type(e)}, text {str(e)}")
        raise
    return urls


def check_url(url):
    url_is_alive = False
    try:
        r = requests.get(url)
    except requests.exceptions.ConnectionError:
        print(f"Could not connect to '{url}'")
    except Exception as e:
        print(f"Exception: {type(e)}, text {str(e)}")
    else:
        if r.status_code not in range(400, 600):
            url_is_alive = True
    return url_is_alive


FILENAME = "urls.txt"
urls = load_urls(FILENAME)
for url in urls:
    url_is_alive = check_url(url)
    if url_is_alive:
        print(f"URL {url} is alive")
    else:
        print(f"URL {url} is dead or is not an URL at all")
        exit(1)

# Devops Assignments

This is a repo to store the solutions to junior devops course assignments.

# Usage

`python <assignment_name>`

Install requirements from `requirements.txt` beforehand (e.g. `pip install -r requirements.txt`).

# Assignments

## Assignment 1

Get the list of urls, check if all of these urls are alive (meaning they do not return a code of 4XX or 5XX). The script expects a file `urls.txt` to be in the current working directory. Exits with code 0 if all urls are alive, otherwise exits with code 1.

## Assignment 2.1

Open the nginx log and print the ip that had the most requests from that log. The script expects a file `log.log` to be in the current working directory.

## Assignment 2.2

Open the nginx log and print top 10 ips, sorted descending by amount of traffic for them. The script expects a file `log.log` to be in the current working directory.
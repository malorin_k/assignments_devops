FILENAME = "log.log"

try:
    with open(FILENAME, 'r') as logfile:
        ips = [line.strip().split(" ")[0] for line in logfile.readlines()]
except (FileNotFoundError, OSError):
    print(f"Could not open file {FILENAME}, terminating")
    exit(1)

ips_count = dict()
for ip in ips:
    ips_count[ip] = ips_count.get(ip, 0) + 1

max_ip = max(ips_count.items(), key=lambda x: x[1])
print(max_ip[0])
